<script>
function flattenDeepArray(arr, flattenedArr) {
	var length = arr.length;

  	for(let i = 0; i < length; i++) {
    	if(Array.isArray(arr[i])) {
      		flattenDeepArray(arr[i], flattenedArr);
    	} 
    	else {
      		flattenedArr.push(arr[i]);
    	}
  	}

  	return flattenedArr;
}

var arr = [1, 2, [3, 4, 5], [6, 7]];

document.write(arr, '=>', flattenDeepArray(arr, [])); // [1, 2, [3, 4, 5], [6, 7]] => [1,2,3,4,5,6,7]

</script>